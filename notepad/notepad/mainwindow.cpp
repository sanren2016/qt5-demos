#include "mainwindow.h"

#include <QDebug>
#include <QMenu>
#include <QMenuBar>
#include <QAction>
#include <QKeyEvent>

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent) {
    init();
}

MainWindow::~MainWindow() {
}


void MainWindow::init() {
    resize(1300, 800);
    setWindowTitle("notepad");
    init_menu();

}
void MainWindow::init_menu() {
    menu_bar = menuBar();
    setMenuBar(menu_bar);
    init_menu_file();
    init_menu_edit();
    init_menu_search();
    init_menu_view();
    init_menu_encoding();
    init_menu_language();
    init_menu_setting();
    init_menu_tool();
    init_menu_macro();
    init_menu_run();
    init_menu_plugin();
    init_menu_window();
}


void MainWindow::init_menu_file() {
    menu_file = new QMenu("文件(&F)");
    menu_bar->addMenu(menu_file);


    act_file_new = new QAction("新建");
    menu_file->addAction(act_file_new);
    act_file_new->setShortcut(Qt::CTRL | Qt::Key_N);
    connect(act_file_new, &QAction::triggered, [&]() {
        qDebug() << "[action] [menu->file->act_file_new]";
    });

    menu_file->addSeparator();

    act_file_open = new QAction("打开");
    act_file_open->setShortcut(Qt::CTRL | Qt::Key_O);
    menu_file->addAction(act_file_open);
    connect(act_file_open, &QAction::triggered, [&]() {
        qDebug() << "[action] [menu->file->act_file_open]";
    });

    menu_file_open_dir = new QMenu("打开所在文件夹");
    menu_file->addMenu(menu_file_open_dir);

    act_file_open_dir_explore = new QAction("文件管理器");
    menu_file_open_dir->addAction(act_file_open_dir_explore);
    connect(act_file_open_dir_explore, &QAction::triggered, [&]() {
        qDebug() << "[action] [menu->file->act_file_open_dir_explore]";
    });

    act_file_open_dir_shell = new QAction("命令行");
    menu_file_open_dir->addAction(act_file_open_dir_shell);
    connect(act_file_open_dir_shell, &QAction::triggered, [&]() {
        qDebug() << "[action] [menu->file->act_file_open_dir_shell]";
    });


    act_file_open_with_default = new QAction("使用默认查看器打开");
    menu_file->addAction(act_file_open_with_default);
    connect(act_file_open_with_default, &QAction::triggered, [&]() {
        qDebug() << "[action] [menu->file->act_file_open_with_default]";
    });

    act_file_open_dir_as_workspace = new QAction("打开文件夹为工作区");
    menu_file->addAction(act_file_open_dir_as_workspace);
    connect(act_file_open_dir_as_workspace, &QAction::triggered, [&]() {
        qDebug() << "[action] [menu->file->act_file_open_dir_as_workspace]";
    });
    \
    act_file_reread = new QAction("重新读取文件");
    act_file_reread->setShortcut(Qt::CTRL | Qt::Key_R);
    menu_file->addAction(act_file_reread);
    connect(act_file_reread, &QAction::triggered, [&]() {
        qDebug() << "[action] [menu->file->act_file_reread]";
    });

    menu_file->addSeparator();

    act_file_save = new QAction("保存");
    act_file_save->setShortcut(Qt::CTRL | Qt::Key_S);
    menu_file->addAction(act_file_save);
    connect(act_file_save, &QAction::triggered, [&]() {
        qDebug() << "[action] [menu->file->act_file_save]";
    });

    act_file_save_as = new QAction("另存为");
    act_file_save_as->setShortcut(Qt::CTRL | Qt::ALT | Qt::Key_S);
    menu_file->addAction(act_file_save_as);
    connect(act_file_save_as, &QAction::triggered, [&]() {
        act_save_as();
    });

    act_file_save_all = new QAction("全部保存");
    act_file_save_all->setShortcut(Qt::CTRL | Qt::SHIFT | Qt::Key_S);
    menu_file->addAction(act_file_save_all);
    connect(act_file_save_all, &QAction::triggered, [&]() {
        act_save_all();
    });

    act_file_rename = new QAction("重命名");
    menu_file->addAction(act_file_rename);
    connect(act_file_rename, &QAction::triggered, [&]() {
        qDebug() << "[action] [menu->file->act_file_rename]";
    });

    menu_file->addSeparator();

    act_file_close = new QAction("关闭");
    act_file_close->setShortcut(Qt::CTRL | Qt::Key_W);
    menu_file->addAction(act_file_close);
    connect(act_file_close, &QAction::triggered, [&]() {
        qDebug() << "[action] [menu->file->act_file_close]";
    });

    act_file_close_all = new QAction("全部关闭");
    act_file_close_all->setShortcut(Qt::CTRL | Qt::SHIFT | Qt::Key_W);
    menu_file->addAction(act_file_close_all);
    connect(act_file_close_all, &QAction::triggered, [&]() {
        act_close_all();
    });

    menu_file_close_more = new QMenu("更多关闭方式");
    menu_file->addMenu(menu_file_close_more);

    act_file_close_more_without_curr = new QAction("关闭非当前文件");
    menu_file_close_more->addAction(act_file_close_more_without_curr);
    connect(act_file_close_more_without_curr, &QAction::triggered, [&]() {
        qDebug() << "[action] [menu->file->act_file_close_more_without_curr]";
    });

    act_file_close_more_left = new QAction("关闭左侧文件");
    menu_file_close_more->addAction(act_file_close_more_left);
    connect(act_file_close_more_left, &QAction::triggered, [&]() {
        qDebug() << "[action] [menu->file->act_file_close_more_left]";
    });

    act_file_close_more_right = new QAction("关闭右侧文件");
    menu_file_close_more->addAction(act_file_close_more_right);
    connect(act_file_close_more_right, &QAction::triggered, [&]() {
        qDebug() << "[action] [menu->file->act_file_close_more_right]";
    });

    act_file_close_more_not_saved = new QAction("关闭所有未修改文件");
    menu_file_close_more->addAction(act_file_close_more_not_saved);
    connect(act_file_close_more_not_saved, &QAction::triggered, [&]() {
        qDebug() << "[action] [menu->file->act_file_close_more_not_saved]";
    });

    menu_file->addSeparator();

    act_file_delete_disk = new QAction("从磁盘删除");
    menu_file->addAction(act_file_delete_disk);
    connect(act_file_delete_disk, &QAction::triggered, [&]() {
        qDebug() << "[action] [menu->file->act_file_delete_disk]";
    });

    menu_file->addSeparator();

    act_file_load_session = new QAction("读取会话");
    menu_file->addAction(act_file_load_session);
    connect(act_file_load_session, &QAction::triggered, [&]() {
        qDebug() << "[action] [menu->file->act_file_load_session]";
    });

    act_file_save_session = new QAction("保存会话");
    menu_file->addAction(act_file_save_session);
    connect(act_file_save_session, &QAction::triggered, [&]() {
        qDebug() << "[action] [menu->file->act_file_save_session]";
    });

    menu_file->addSeparator();

    act_file_reopen_last_file = new QAction("恢复最近关闭的文件");
    act_file_reopen_last_file->setShortcut(Qt::CTRL | Qt::SHIFT | Qt::Key_T);
    menu_file->addAction(act_file_reopen_last_file);
    connect(act_file_reopen_last_file, &QAction::triggered, [&]() {
        act_reopen_last_file();
    });

    act_file_open_file_list = new QAction("打开文件列表");
    menu_file->addAction(act_file_open_file_list);
    connect(act_file_open_file_list, &QAction::triggered, [&]() {
        qDebug() << "[action] [menu->file->act_file_open_file_list]";
    });

    act_file_close_file_list = new QAction("清除文件列表");
    menu_file->addAction(act_file_close_file_list);
    connect(act_file_close_file_list, &QAction::triggered, [&]() {
        qDebug() << "[action] [menu->file->act_file_close_file_list]";
    });

    menu_file->addSeparator();

    act_file_exit = new QAction("退出");
    menu_file->addAction(act_file_exit);
    connect(act_file_exit, &QAction::triggered, [&]() {
        qDebug() << "[action] [menu->file->act_file_exit]";
    });
}


void MainWindow::init_menu_edit() {
    menu_edit = new QMenu("编辑(&E)");
    menu_bar->addMenu(menu_edit);





    act_edit_undo = new QAction("撤销");
    act_edit_undo->setShortcut(Qt::CTRL | Qt::Key_Z);
    menu_edit->addAction(act_edit_undo);
    connect(act_edit_undo, &QAction::triggered, [&]() {
        qDebug() << "[action] [menu->edit->act_edit_undo]";
    });

    act_edit_redo = new QAction("恢复");
    act_edit_redo->setShortcut(Qt::CTRL | Qt::Key_Y);
    menu_edit->addAction(act_edit_redo);
    connect(act_edit_redo, &QAction::triggered, [&]() {
        qDebug() << "[action] [menu->edit->act_edit_redo]";
    });

    menu_edit->addSeparator();

    act_edit_cut = new QAction("剪切");
    act_edit_cut->setShortcut(Qt::CTRL | Qt::Key_X);
    menu_edit->addAction(act_edit_cut);
    connect(act_edit_cut, &QAction::triggered, [&]() {
        qDebug() << "[action] [menu->edit->act_edit_cut]";
    });

    act_edit_copy = new QAction("复制");
    act_edit_copy->setShortcut(Qt::CTRL | Qt::Key_C);
    menu_edit->addAction(act_edit_copy);
    connect(act_edit_copy, &QAction::triggered, [&]() {
        qDebug() << "[action] [menu->edit->act_edit_copy]";
    });

    act_edit_paste = new QAction("黏贴");
    act_edit_paste->setShortcut(Qt::CTRL | Qt::Key_V);
    menu_edit->addAction(act_edit_paste);
    connect(act_edit_paste, &QAction::triggered, [&]() {
        qDebug() << "[action] [menu->edit->act_edit_paste]";
    });

    act_edit_select_all = new QAction("全选");
    act_edit_select_all->setShortcut(Qt::CTRL | Qt::Key_A);
    menu_edit->addAction(act_edit_select_all);
    connect(act_edit_select_all, &QAction::triggered, [&]() {
        qDebug() << "[action] [menu->edit->act_edit_select_all]";
    });

    act_edit_start_end_select = new QAction("开始/结束选择");

    menu_edit->addSeparator();

    menu_edit_copy_clipboard = new QMenu("复制到剪切板");
    menu_edit->addMenu(menu_edit_copy_clipboard);

    act_edit_copy_clipboard_file_path = new QAction("复制当前文件路径");
    menu_edit_copy_clipboard->addAction(act_edit_copy_clipboard_file_path);
    connect(act_edit_copy_clipboard_file_path, &QAction::triggered, [&]() {
        qDebug() << "[action] [menu->edit->act_edit_copy_clipboard_file_path]";
    });
    act_edit_copy_clipboard_file_name = new QAction("复制当前文件名");
    menu_edit_copy_clipboard->addAction(act_edit_copy_clipboard_file_name);
    connect(act_edit_copy_clipboard_file_name, &QAction::triggered, [&]() {
        qDebug() << "[action] [menu->edit->act_edit_copy_clipboard_file_name]";
    });
    act_edit_copy_clipboard_file_dir = new QAction("复制当前目录路径");
    menu_edit_copy_clipboard->addAction(act_edit_copy_clipboard_file_dir);
    connect(act_edit_copy_clipboard_file_dir, &QAction::triggered, [&]() {
        qDebug() << "[action] [menu->edit->act_edit_copy_clipboard_file_dir]";
    });

    menu_edit_indent = new QMenu("缩进");
    menu_edit->addMenu(menu_edit_indent);

    act_edit_indent_tab = new QAction("插入制表符(缩进)");
    menu_edit_indent->addAction(act_edit_indent_tab);
    connect(act_edit_indent_tab, &QAction::triggered, [&]() {
        qDebug() << "[action] [menu->edit->act_edit_indent_tab]";
    });


    act_edit_indent_space = new QAction("插入制表符(空格)");
    menu_edit_indent->addAction(act_edit_indent_space);
    connect(act_edit_indent_space, &QAction::triggered, [&]() {
        qDebug() << "[action] [menu->edit->act_edit_indent_space]";
    });

    menu_edit_change_case  = new QMenu("转换大小写");
    menu_edit->addMenu(menu_edit_change_case);


    QAction *act_edit_change_case_upper = new QAction("转成大写");
    act_edit_change_case_upper->setShortcut(Qt::CTRL | Qt::SHIFT | Qt::Key_U);
    connect(act_edit_change_case_upper, &QAction::triggered, [&]() {
        qDebug() << "[action] [menu->edit->act_edit_change_case_upper]";
    });
    menu_edit_change_case->addAction(act_edit_change_case_upper);


    act_edit_change_case_lower = new QAction("转成小写");
    act_edit_change_case_lower->setShortcut(Qt::CTRL | Qt::Key_U);
    connect(act_edit_change_case_lower, &QAction::triggered, [&]() {
        qDebug() << "[action] [menu->edit->act_edit_change_case_lower]";
    });
    menu_edit_change_case->addAction(act_edit_change_case_lower);

    act_edit_change_case_first_upper_only = new QAction("每词转成仅首字母大写");
    act_edit_change_case_first_upper_only->setShortcut(Qt::ALT | Qt::Key_U);
    connect(act_edit_change_case_first_upper_only, &QAction::triggered, [&]() {
        qDebug() << "[action] [menu->edit->act_edit_change_case_first_upper_only]";
    });
    menu_edit_change_case->addAction(act_edit_change_case_first_upper_only);

    act_edit_change_case_first_upper = new QAction("每词首字母转成大写");
    act_edit_change_case_first_upper->setShortcut(Qt::ALT | Qt::SHIFT | Qt::Key_U);
    connect(act_edit_change_case_first_upper, &QAction::triggered, [&]() {
        qDebug() << "[action] [menu->edit->act_edit_change_case_first_upper]";
    });
    menu_edit_change_case->addAction(act_edit_change_case_first_upper);

    act_edit_change_case_first_lower_only = new QAction("每词转成仅首字母小写");
    act_edit_change_case_first_lower_only->setShortcut(Qt::CTRL | Qt::ALT | Qt::Key_U);
    connect(act_edit_change_case_first_lower_only, &QAction::triggered, [&]() {
        qDebug() << "[action] [menu->edit->act_edit_change_case_first_lower_only]";
    });
    menu_edit_change_case->addAction(act_edit_change_case_first_lower_only);

    act_edit_change_case_first_lower = new QAction("每词首字母转成小写");
    act_edit_change_case_first_lower->setShortcut(Qt::CTRL | Qt::ALT | Qt::SHIFT | Qt::Key_U);
    connect(act_edit_change_case_first_lower, &QAction::triggered, [&]() {
        qDebug() << "[action] [menu->edit->act_edit_change_case_first_lower]";
    });
    menu_edit_change_case->addAction(act_edit_change_case_first_lower);


    act_edit_change_case_switch = new QAction("大小写互换");
    connect(act_edit_change_case_switch, &QAction::triggered, [&]() {
        qDebug() << "[action] [menu->edit->act_edit_change_case_switch]";
    });
    menu_edit_change_case->addAction(act_edit_change_case_switch);






    menu_edit_line  = new QMenu("行操作");
    menu_edit->addMenu(menu_edit_line);


    act_edit_line_copy = new QAction("复制当前行");
    act_edit_line_copy->setShortcut(Qt::CTRL | Qt::Key_D);
    connect(act_edit_line_copy, &QAction::triggered, [&]() {
        qDebug() << "[action] [menu->edit->act_edit_line_copy]";
    });
    menu_edit_line->addAction(act_edit_line_copy);


    act_edit_line_delete_continuous = new QAction("删除连续的重复行");
    connect(act_edit_line_delete_continuous, &QAction::triggered, [&]() {
        qDebug() << "[action] [menu->edit->act_edit_line_delete_continuous]";
    });
    menu_edit_line->addAction(act_edit_line_delete_continuous);


    act_edit_line_split = new QAction("分割行");
    act_edit_line_split->setShortcut(Qt::CTRL | Qt::Key_I);
    connect(act_edit_line_split, &QAction::triggered, [&]() {
        qDebug() << "[action] [menu->edit->act_edit_line_split]";
    });
    menu_edit_line->addAction(act_edit_line_split);


    act_edit_line_merge = new QAction("合并行");
    act_edit_line_merge->setShortcut(Qt::CTRL | Qt::Key_J);
    connect(act_edit_line_merge, &QAction::triggered, [&]() {
        qDebug() << "[action] [menu->edit->act_edit_line_merge]";
    });
    menu_edit_line->addAction(act_edit_line_merge);


    act_edit_line_move_up = new QAction("上移当前行");
    act_edit_line_move_up->setShortcut(Qt::CTRL | Qt::SHIFT | Qt::Key_Up);
    connect(act_edit_line_move_up, &QAction::triggered, [&]() {
        qDebug() << "[action] [menu->edit->act_edit_line_move_up]";
    });
    menu_edit_line->addAction(act_edit_line_move_up);


    act_edit_line_move_down = new QAction("下移当前行");
    act_edit_line_move_down->setShortcut(Qt::CTRL | Qt::SHIFT | Qt::Key_Down);
    connect(act_edit_line_move_down, &QAction::triggered, [&]() {
        qDebug() << "[action] [menu->edit->act_edit_line_move_down]";
    });
    menu_edit_line->addAction(act_edit_line_move_down);


    act_edit_line_delete_empty = new QAction("移除空行");
    connect(act_edit_line_delete_empty, &QAction::triggered, [&]() {
        qDebug() << "[action] [menu->edit->act_edit_line_delete_empty]";
    });
    menu_edit_line->addAction(act_edit_line_delete_empty);


    act_edit_line_delete_empty_with_char = new QAction("移除空行(包括空白字符)");
    connect(act_edit_line_delete_empty_with_char, &QAction::triggered, [&]() {
        qDebug() << "[action] [menu->edit->act_edit_line_delete_empty_with_char]";
    });
    menu_edit_line->addAction(act_edit_line_delete_empty_with_char);


    act_edit_line_insert_up = new QAction("在当前行上方插入空行");
    act_edit_line_insert_up->setShortcut(Qt::CTRL | Qt::ALT | Qt::Key_Enter);
    connect(act_edit_line_insert_up, &QAction::triggered, [&]() {
        qDebug() << "[action] [menu->edit->act_edit_line_insert_up]";
    });
    menu_edit_line->addAction(act_edit_line_insert_up);


    act_edit_line_insert_down = new QAction("在当前行下方插入空行");
    act_edit_line_insert_down->setShortcut(Qt::CTRL | Qt::ALT | Qt::SHIFT | Qt::Key_Enter);
    connect(act_edit_line_insert_down, &QAction::triggered, [&]() {
        qDebug() << "[action] [menu->edit->act_edit_line_insert_down]";
    });
    menu_edit_line->addAction(act_edit_line_insert_down);

    menu_edit_line->addSeparator();


    act_edit_line_asc = new QAction("升序排列文本行");
    connect(act_edit_line_asc, &QAction::triggered, [&]() {
        qDebug() << "[action] [menu->edit->act_edit_line_asc]";
    });
    menu_edit_line->addAction(act_edit_line_asc);


    act_edit_line_asc_number = new QAction("升序排列整数");
    connect(act_edit_line_asc_number, &QAction::triggered, [&]() {
        qDebug() << "[action] [menu->edit->act_edit_line_asc_number]";
    });
    menu_edit_line->addAction(act_edit_line_asc_number);


    act_edit_line_asc_decimals_by_camma = new QAction("升序排列小数(逗号作为小数点)");
    connect(act_edit_line_asc_decimals_by_camma, &QAction::triggered, [&]() {
        qDebug() << "[action] [menu->edit->act_edit_line_asc_decimals_by_camma]";
    });
    menu_edit_line->addAction(act_edit_line_asc_decimals_by_camma);


    act_edit_line_asc_decimals_by_dot = new QAction("升序排列小数(句点作为小数点)");
    connect(act_edit_line_asc_decimals_by_dot, &QAction::triggered, [&]() {
        qDebug() << "[action] [menu->edit->act_edit_line_asc_decimals_by_dot]";
    });
    menu_edit_line->addAction(act_edit_line_asc_decimals_by_dot);

    menu_edit_line->addSeparator();


    act_edit_line_desc = new QAction("降序排列文本行");
    connect(act_edit_line_desc, &QAction::triggered, [&]() {
        qDebug() << "[action] [menu->edit->act_edit_line_desc]";
    });
    menu_edit_line->addAction(act_edit_line_desc);


    act_edit_line_desc_number = new QAction("降序排列证书");
    connect(act_edit_line_desc_number, &QAction::triggered, [&]() {
        qDebug() << "[action] [menu->edit->act_edit_line_desc_number]";
    });
    menu_edit_line->addAction(act_edit_line_desc_number);


    act_edit_line_desc_decimals_by_camma = new QAction("降序排列小数(逗号作为小数点)");
    connect(act_edit_line_desc_decimals_by_camma, &QAction::triggered, [&]() {
        qDebug() << "[action] [menu->edit->act_edit_line_desc_decimals_by_camma]";
    });
    menu_edit_line->addAction(act_edit_line_desc_decimals_by_camma);


    act_edit_line_desc_decimals_by_dot = new QAction("降序排列小数(句点作为小数点)");
    connect(act_edit_line_desc_decimals_by_dot, &QAction::triggered, [&]() {
        qDebug() << "[action] [menu->edit->act_edit_line_desc_decimals_by_dot]";
    });
    menu_edit_line->addAction(act_edit_line_desc_decimals_by_dot);

    menu_edit_line->addSeparator();

    act_edit_line_random = new QAction("随机排列行");
    connect(act_edit_line_random, &QAction::triggered, [&]() {
        qDebug() << "[action] [menu->edit->act_edit_line_random]";
    });
    menu_edit_line->addAction(act_edit_line_random);




    menu_edit_comment  = new QMenu("注释/取消注释");
    menu_edit->addMenu(menu_edit_comment);


    act_edit_comment_comment = new QAction("添加/删除行注释");
    act_edit_comment_comment->setShortcut(Qt::CTRL | Qt::Key_Q);
    connect(act_edit_comment_comment, &QAction::triggered, [&]() {
        qDebug() << "[action] [menu->edit->act_edit_comment_comment]";
    });
    menu_edit_comment->addAction(act_edit_comment_comment);

    act_edit_comment_line = new QAction("设置行注释");
    act_edit_comment_line->setShortcut(Qt::CTRL |  Qt::Key_K);
    connect(act_edit_comment_line, &QAction::triggered, [&]() {
        qDebug() << "[action] [menu->edit->act_edit_comment_line]";
    });
    menu_edit_comment->addAction(act_edit_comment_line);

    act_edit_comment_line_cancle = new QAction("取消行注释");
    act_edit_comment_line_cancle->setShortcut(Qt::CTRL | Qt::SHIFT | Qt::Key_K);
    connect(act_edit_comment_line_cancle, &QAction::triggered, [&]() {
        qDebug() << "[action] [menu->edit->act_edit_comment_line_cancle]";
    });
    menu_edit_comment->addAction(act_edit_comment_line_cancle);

    act_edit_comment_block = new QAction("区块注释");
    act_edit_comment_block->setShortcut(Qt::CTRL | Qt::SHIFT | Qt::Key_Q);
    connect(act_edit_comment_block, &QAction::triggered, [&]() {
        qDebug() << "[action] [menu->edit->act_edit_comment_block]";
    });
    menu_edit_comment->addAction(act_edit_comment_block);

    act_edit_comment_block_cancle = new QAction("清除区块注释");
    connect(act_edit_comment_block_cancle, &QAction::triggered, [&]() {
        qDebug() << "[action] [menu->edit->act_edit_comment_block_cancle]";
    });
    menu_edit_comment->addAction(act_edit_comment_block_cancle);



    menu_edit_complete  = new QMenu("自动完成");
    menu_edit->addMenu(menu_edit_complete);

    act_edit_complete_func = new QAction("函数自动完成");
    act_edit_complete_func->setShortcut(Qt::ALT | Qt::Key_Slash);
    connect(act_edit_complete_func, &QAction::triggered, [&]() {
        qDebug() << "[action] [menu->edit->act_edit_complete_func]";
    });
    menu_edit_complete->addAction(act_edit_complete_func);

    act_edit_complete_word = new QAction("单词自动完成");
    act_edit_complete_word->setShortcut(Qt::CTRL | Qt::Key_Enter);
    connect(act_edit_complete_word, &QAction::triggered, [&]() {
        qDebug() << "[action] [menu->edit->act_edit_complete_word]";
    });
    menu_edit_complete->addAction(act_edit_complete_word);

    act_edit_complete_param = new QAction("函数参数提示");
    act_edit_complete_param->setShortcut(Qt::CTRL | Qt::SHIFT | Qt::Key_Slash);
    connect(act_edit_complete_param, &QAction::triggered, [&]() {
        qDebug() << "[action] [menu->edit->act_edit_complete_param]";
    });
    menu_edit_complete->addAction(act_edit_complete_param);

    act_edit_complete_path = new QAction("路径补全");
    act_edit_complete_path->setShortcut(Qt::CTRL | Qt::ALT | Qt::Key_Slash);
    connect(act_edit_complete_path, &QAction::triggered, [&]() {
        qDebug() << "[action] [menu->edit->act_edit_complete_path]";
    });
    menu_edit_complete->addAction(act_edit_complete_path);

    menu_edit_change_format = new QMenu("文档格式转换");
    menu_edit->addMenu(menu_edit_change_format);

    act_edit_change_format_windows = new QAction("转为 Windows (CR LF)");
    connect(act_edit_change_format_windows, &QAction::triggered, [&]() {
        qDebug() << "[action] [menu->edit->act_edit_change_format_windows]";
    });
    menu_edit_change_format->addAction(act_edit_change_format_windows);

    act_edit_change_format_unix = new QAction("转为 Unix (LF)");
    connect(act_edit_change_format_unix, &QAction::triggered, [&]() {
        qDebug() << "[action] [menu->edit->act_edit_change_format_unix]";
    });
    menu_edit_change_format->addAction(act_edit_change_format_unix);


    act_edit_change_format_macintosh = new QAction("转为 Macintosh (CR)");
    connect(act_edit_change_format_macintosh, &QAction::triggered, [&]() {
        qDebug() << "[action] [menu->edit->act_edit_change_format_macintosh]";
    });
    menu_edit_change_format->addAction(act_edit_change_format_macintosh);




    menu_edit_empty = new QMenu("空白字符操作");
    menu_edit->addMenu(menu_edit_empty);

    act_edit_empty_remove_space_end = new QAction("移除行尾空格");
    connect(act_edit_empty_remove_space_end, &QAction::triggered, [&]() {
        qDebug() << "[action] [menu->edit->act_edit_empty_remove_space_end]";
    });
    menu_edit_empty->addAction(act_edit_empty_remove_space_end);

    act_edit_empty_remove_space_start = new QAction("移除行首空格");
    connect(act_edit_empty_remove_space_start, &QAction::triggered, [&]() {
        qDebug() << "[action] [menu->edit->act_edit_empty_remove_space_start]";
    });
    menu_edit_empty->addAction(act_edit_empty_remove_space_start);

    act_edit_empty_remove_space_start_and_end = new QAction("移除行首和行尾空格");
    connect(act_edit_empty_remove_space_start_and_end, &QAction::triggered, [&]() {
        qDebug() << "[action] [menu->edit->act_edit_empty_remove_space_start_and_end]";
    });
    menu_edit_empty->addAction(act_edit_empty_remove_space_start_and_end);

    act_edit_empty_eof_to_space = new QAction("EOL转空格");
    connect(act_edit_empty_eof_to_space, &QAction::triggered, [&]() {
        qDebug() << "[action] [menu->edit->act_edit_empty_eof_to_space]";
    });
    menu_edit_empty->addAction(act_edit_empty_eof_to_space);

    act_edit_empty_remove_unnecessary_space_and_eof = new QAction("移除非必须的空白和EOL");
    connect(act_edit_empty_remove_unnecessary_space_and_eof, &QAction::triggered, [&]() {
        qDebug() << "[action] [menu->edit->act_edit_empty_remove_unnecessary_space_and_eof]";
    });
    menu_edit_empty->addAction(act_edit_empty_remove_unnecessary_space_and_eof);

    menu_edit_empty->addSeparator();

    act_edit_empty_tab_to_space = new QAction("TAB转空格");
    connect(act_edit_empty_tab_to_space, &QAction::triggered, [&]() {
        qDebug() << "[action] [menu->edit->act_edit_empty_tab_to_space]";
    });
    menu_edit_empty->addAction(act_edit_empty_tab_to_space);

    act_edit_empty_space_to_tab = new QAction("空格转TAB(全部)");
    connect(act_edit_empty_space_to_tab, &QAction::triggered, [&]() {
        qDebug() << "[action] [menu->edit->]";
    });
    menu_edit_empty->addAction(act_edit_empty_space_to_tab);

    act_edit_empty_space_to_tab_start = new QAction("空格转TAB(行首)");
    connect(act_edit_empty_space_to_tab_start, &QAction::triggered, [&]() {
        qDebug() << "[action] [menu->edit->act_edit_empty_space_to_tab_start]";
    });
    menu_edit_empty->addAction(act_edit_empty_space_to_tab_start);




    menu_edit_selected_paste = new QMenu("选择性黏贴");
    menu_edit->addMenu(menu_edit_selected_paste);



    edit_selected_paste_html = new QAction("黏贴HTML");
    connect(edit_selected_paste_html, &QAction::triggered, [&]() {
        qDebug() << "[action] [menu->edit->edit_selected_paste_html]";
    });
    menu_edit_selected_paste->addAction(edit_selected_paste_html);

    edit_selected_paste_rtf = new QAction("黏贴RTF");
    connect(edit_selected_paste_rtf, &QAction::triggered, [&]() {
        qDebug() << "[action] [menu->edit->]";
    });
    menu_edit_selected_paste->addAction(edit_selected_paste_rtf);

    menu_edit_selected_paste->addSeparator();

    edit_selected_paste_bin_copy = new QAction("复制二进制");
    connect(edit_selected_paste_bin_copy, &QAction::triggered, [&]() {
        qDebug() << "[action] [menu->edit->edit_selected_paste_bin_copy]";
    });
    menu_edit_selected_paste->addAction(edit_selected_paste_bin_copy);


    edit_selected_paste_bin_cut = new QAction("剪切二进制");
    connect(edit_selected_paste_bin_cut, &QAction::triggered, [&]() {
        qDebug() << "[action] [menu->edit->edit_selected_paste_bin_cut]";
    });
    menu_edit_selected_paste->addAction(edit_selected_paste_bin_cut);

    edit_selected_paste_bin_paste = new QAction("黏贴二进制");
    connect(edit_selected_paste_bin_paste, &QAction::triggered, [&]() {
        qDebug() << "[action] [menu->edit->edit_selected_paste_bin_paste]";
    });
    menu_edit_selected_paste->addAction(edit_selected_paste_bin_paste);





    menu_edit_selected_op = new QMenu("对选中内容操作");
    menu_edit->addMenu(menu_edit_selected_op);

    act_edit_selected_op_open_file = new QAction("打开文件");
    connect(act_edit_selected_op_open_file, &QAction::triggered, [&]() {
        qDebug() << "[action] [menu->edit->act_edit_selected_op_open_file]";
    });
    menu_edit_selected_op->addAction(act_edit_selected_op_open_file);

    act_edit_selected_op_open_explore = new QAction("在文件管理器中打开包含文件");
    connect(act_edit_selected_op_open_explore, &QAction::triggered, [&]() {
        qDebug() << "[action] [menu->edit->act_edit_selected_op_open_explore]";
    });
    menu_edit_selected_op->addAction(act_edit_selected_op_open_explore);

    menu_edit_selected_op->addSeparator();

    act_edit_selected_op_search = new QAction("在线搜索");
    connect(act_edit_selected_op_search, &QAction::triggered, [&]() {
        qDebug() << "[action] [menu->edit->act_edit_selected_op_search]";
    });
    menu_edit_selected_op->addAction(act_edit_selected_op_search);

    act_edit_selected_op_change_search_engine = new QAction("更改搜索引擎");
    connect(act_edit_selected_op_change_search_engine, &QAction::triggered, [&]() {
        qDebug() << "[action] [menu->edit->act_edit_selected_op_change_search_engine]";
    });
    menu_edit_selected_op->addAction(act_edit_selected_op_change_search_engine);


    menu_edit->addSeparator();



    act_edit_column_model = new QAction("列快模式");
    connect(act_edit_column_model, &QAction::triggered, [&]() {
        qDebug() << "[action] [menu->edit->act_edit_column_model]";
    });
    menu_edit->addAction(act_edit_column_model);


    act_edit_column_edit = new QAction("列快编辑");
    act_edit_column_edit->setShortcut(Qt::ALT | Qt::Key_C);
    connect(act_edit_column_edit, &QAction::triggered, [&]() {
        qDebug() << "[action] [menu->edit->act_edit_column_edit]";
    });
    menu_edit->addAction(act_edit_column_edit);

    act_edit_char_panel = new QAction("字符面板");
    connect(act_edit_char_panel, &QAction::triggered, [&]() {
        qDebug() << "[action] [menu->edit->act_edit_char_panel]";
    });
    menu_edit->addAction(act_edit_char_panel);

    act_edit_clipboard_history = new QAction("历史剪切板");
    connect(act_edit_clipboard_history, &QAction::triggered, [&]() {
        qDebug() << "[action] [menu->edit->act_edit_clipboard_history]";
    });
    menu_edit->addAction(act_edit_clipboard_history);

    menu_edit->addSeparator();

    act_edit_set_readonly = new QAction("设为只读");
    connect(act_edit_set_readonly, &QAction::triggered, [&]() {
        qDebug() << "[action] [menu->edit->act_edit_set_readonly]";
    });
    menu_edit->addAction(act_edit_set_readonly);

    act_edit_clear_readonly = new QAction("清除只读标记");
    connect(act_edit_clear_readonly, &QAction::triggered, [&]() {
        qDebug() << "[action] [menu->edit->act_edit_clear_readonly]";
    });
    menu_edit->addAction(act_edit_clear_readonly);

}


void MainWindow::init_menu_search() {
    menu_search = new QMenu("搜索(&S)");
    menu_bar->addMenu(menu_search);
}


void MainWindow::init_menu_view() {
    menu_view = new QMenu("视图(&V)");
    menu_bar->addMenu(menu_view);
}


void MainWindow::init_menu_encoding() {
    menu_encoding = new QMenu("编码(&N)");
    menu_bar->addMenu(menu_encoding);
}


void MainWindow::init_menu_language() {
    menu_language = new QMenu("语言(&L)");
    menu_bar->addMenu(menu_language);
}


void MainWindow::init_menu_setting() {
    menu_setting = new QMenu("设置(&T)");
    menu_bar->addMenu(menu_setting);
}


void MainWindow::init_menu_tool() {
    menu_tool = new QMenu("工具(&O)");
    menu_bar->addMenu(menu_tool);
}


void MainWindow::init_menu_macro() {
    menu_macro = new QMenu("宏(&)");
    menu_bar->addMenu(menu_macro);
}


void MainWindow::init_menu_run() {
    menu_run = new QMenu("运行(&M)");
    menu_bar->addMenu(menu_run);
}


void MainWindow::init_menu_plugin() {
    menu_plugin = new QMenu("插件(&P)");
    menu_bar->addMenu(menu_plugin);
}


void MainWindow::init_menu_window() {
    menu_window = new QMenu("窗口(&W)");
    menu_bar->addMenu(menu_window);
}

void MainWindow::keyPressEvent(QKeyEvent *e) {

    // 另存为
    if (e->modifiers() == (Qt::ControlModifier | Qt::ShiftModifier) && (e->key() == Qt::Key_S)) {
        act_save_as();
    } else if (e->modifiers() == (Qt::ControlModifier | Qt::AltModifier) && (e->key() == Qt::Key_S)) {
        // 全部保存
        act_save_all();
    } else if (e->modifiers() == (Qt::ControlModifier | Qt::ShiftModifier) && (e->key() == Qt::Key_W)) {
        // 全部关闭
        act_close_all();
    } else if (e->modifiers() == (Qt::ControlModifier | Qt::ShiftModifier) && (e->key() == Qt::Key_T)) {
        // 打开最近关闭的文件
        act_reopen_last_file();
    }
}

void MainWindow::act_save_as() {
    qDebug() << "[action] [menu->file->act_file_save_as]";
}

void MainWindow::act_save_all() {
    qDebug() << "[action] [menu->file->act_file_save_all]";
}

void MainWindow::act_close_all() {
    qDebug() << "[action] [menu->file->act_file_close_all]";
}

void MainWindow::act_reopen_last_file() {
    qDebug() << "[action] [menu->file->act_file_reopen_last_file]";
}




