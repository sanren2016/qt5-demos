#include "mainwindow.h"

#include <QApplication>

/**
 * khl Ctrl+Shift+S 和 Ctrl+Alt+S 冲突
 * @brief 主函数
 * @param argc argc
 * @param argv argv
 * @return 0
 */
int main(int argc, char *argv[]) {
    QApplication a(argc, argv);
    MainWindow w;
    w.show();
    return a.exec();
}
