#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>

class QMenu;
class QMenuBar;
class QAction;
class QKeyEvent;


class MainWindow : public QMainWindow {
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

protected:
    virtual void keyPressEvent(QKeyEvent *e);

private:

    /**
     * @brief 另存为
     */
    void act_save_as();

    /**
     * @brief 全部保存
     */
    void act_save_all();

    /**
     * @brief 全部关闭
     */
    void act_close_all();

    /**
     * @brief 打开最近关闭的文件
     */
    void act_reopen_last_file();

    /**
     * @brief 初始化
     */
    void init();

    /**
     * @brief 初始化菜单
     */
    void init_menu();

    /**
     * @brief 初始化文件菜单
     */
    void init_menu_file();
    /**
    * @brief 初始化编辑菜单
    */
    void init_menu_edit();

    /**
    * @brief 初始化搜索菜单
    */
    void init_menu_search();

    /**
    * @brief 初始化视图菜单
    */
    void init_menu_view();

    /**
    * @brief 初始化编码菜单
    */
    void init_menu_encoding();

    /**
    * @brief 初始化语言菜单
    */
    void init_menu_language();

    /**
    * @brief 初始化设置菜单
    */
    void init_menu_setting();

    /**
    * @brief 初始化工具菜单
    */
    void init_menu_tool();

    /**
    * @brief 初始化宏菜单
    */
    void init_menu_macro();

    /**
    * @brief 初始化运行菜单
    */
    void init_menu_run();

    /**
    * @brief 初始化插件菜单
    */
    void init_menu_plugin();

    /**
    * @brief 初始化窗口菜单
    */
    void init_menu_window();

    /**
    * @brief 菜单栏
    */
    QMenuBar *menu_bar;



    /**
    * @brief 菜单: 文件
    */
    QMenu *menu_file;
    /**
    * @brief 菜单项: 新建
    */
    QAction *act_file_new;
    /**
    * @brief 菜单项: 打开
    */
    QAction *act_file_open;
    /**
    * @brief 菜单项: 打开所在文件夹
    */
    QMenu *menu_file_open_dir;
    /**
    * @brief 菜单项: 文件管理器
    */
    QAction *act_file_open_dir_explore;
    /**
    * @brief 菜单项: 命令行
    */
    QAction *act_file_open_dir_shell;
    /**
    * @brief 菜单项: 使用默认查看器打开
    */
    QAction *act_file_open_with_default;
    /**
    * @brief 菜单项: 打开文件夹为工作区
    */
    QAction *act_file_open_dir_as_workspace;
    /**
    * @brief 菜单项: 重新读取文件
    */
    QAction *act_file_reread;
    /**
    * @brief 菜单项: 保存
    */
    QAction *act_file_save;
    /**
    * @brief 菜单项: 另存为
    */
    QAction *act_file_save_as;
    /**
    * @brief 菜单项: 全部保存
    */
    QAction *act_file_save_all;
    /**
    * @brief 菜单项: 重命名
    */
    QAction *act_file_rename;
    /**
    * @brief 菜单项: 关闭
    */
    QAction *act_file_close;
    /**
    * @brief 菜单项: 全部关闭
    */
    QAction *act_file_close_all;
    /**
    * @brief 菜单项: 更多关闭方式
    */
    QMenu *menu_file_close_more;
    /**
    * @brief 菜单项: 关闭非当前文件
    */
    QAction *act_file_close_more_without_curr;
    /**
    * @brief 菜单项: 关闭左侧文件
    */
    QAction *act_file_close_more_left;
    /**
    * @brief 菜单项: 关闭右侧文件
    */
    QAction *act_file_close_more_right;
    /**
    * @brief 菜单项: 关闭所有未修改文件
    */
    QAction *act_file_close_more_not_saved;
    /**
    * @brief 菜单项: 从磁盘删除
    */
    QAction *act_file_delete_disk;
    /**
    * @brief 菜单项: 读取会话
    */
    QAction *act_file_load_session;
    /**
    * @brief 菜单项: 保存会话
    */
    QAction *act_file_save_session;
    /**
    * @brief 菜单项: 恢复最近关闭的文件
    */
    QAction *act_file_reopen_last_file;
    /**
    * @brief 菜单项: 打开文件列表
    */
    QAction *act_file_open_file_list;
    /**
    * @brief 菜单项: 清除文件列表
    */
    QAction *act_file_close_file_list;
    /**
    * @brief 菜单项: 退出
    */
    QAction *act_file_exit;






    /**
    * @brief 菜单: 编辑
    */
    QMenu *menu_edit;


    /**
     * @brief 菜单项: 撤销
     */
    QAction *act_edit_undo;

    /**
     * @brief 菜单项: 恢复
     */
    QAction *act_edit_redo;

    /**
     * @brief 菜单项: 剪切
     */
    QAction *act_edit_cut;

    /**
     * @brief 菜单项:复制
     */
    QAction *act_edit_copy;

    /**
     * @brief 菜单项: 黏贴
     */
    QAction *act_edit_paste;

    /**
     * @brief 菜单项:全选
     */
    QAction *act_edit_select_all;

    /**
     * @brief 菜单项: 开始/结束选择
     */
    QAction *act_edit_start_end_select;

    /**
     * @brief 菜单项: 复制到剪切板
     */
    QMenu *menu_edit_copy_clipboard;

    /**
     * @brief 复制当前文件路径
     */
    QAction *act_edit_copy_clipboard_file_path;

    /**
     * @brief 复制当前文件名
     */
    QAction *act_edit_copy_clipboard_file_name;

    /**
     * @brief 复制当前目录路径
     */
    QAction *act_edit_copy_clipboard_file_dir;

    /**
     * @brief 菜单项: 缩进
     */
    QMenu *menu_edit_indent;


    /**
     * @brief 菜单项: 插入制表符(缩进)
     */
    QAction *act_edit_indent_tab;

    /**
     * @brief 菜单项: 插入制表符(空格)
     */
    QAction *act_edit_indent_space;

    /**
     * @brief 菜单项: 转换大小写
     */
    QMenu *menu_edit_change_case;


    /**
     * @brief 菜单项: 转成大写
     */
    QAction *act_edit_change_case_upper;

    /**
     * @brief 菜单项: 转成小写
     */
    QAction *act_edit_change_case_lower;

    /**
     * @brief 菜单项: 每词转成仅首字母大写
     */
    QAction *act_edit_change_case_first_upper_only;

    /**
     * @brief 菜单项: 每词首字母转成大写
     */
    QAction *act_edit_change_case_first_upper;

    /**
     * @brief 菜单项: 每词转成仅首字母小写
     */
    QAction *act_edit_change_case_first_lower_only;

    /**
     * @brief 菜单项: 每词首字母转成小写
     */
    QAction *act_edit_change_case_first_lower;

    /**
     * @brief 菜单项: 大小写互换
     */
    QAction *act_edit_change_case_switch;




    /**
     * @brief 菜单项: 行操作
     */
    QMenu *menu_edit_line;



    /**
     * @brief 菜单项: 复制当前行
     */
    QAction *act_edit_line_copy;

    /**
     * @brief 菜单项: 删除连续的重复行
     */
    QAction *act_edit_line_delete_continuous;

    /**
     * @brief 菜单项: 分割行
     */
    QAction *act_edit_line_split;

    /**
     * @brief 菜单项: 合并行
     */
    QAction *act_edit_line_merge;

    /**
     * @brief 菜单项: 上移当前行
     */
    QAction *act_edit_line_move_up;

    /**
     * @brief 菜单项: 下移当前行
     */
    QAction *act_edit_line_move_down;

    /**
     * @brief 菜单项: 移除空行
     */
    QAction *act_edit_line_delete_empty;

    /**
     * @brief 菜单项: 移除空行(包括空白字符)
     */
    QAction *act_edit_line_delete_empty_with_char;

    /**
     * @brief 菜单项: 在当前行上方插入空行
     */
    QAction *act_edit_line_insert_up;

    /**
     * @brief 菜单项: 在当前行下方插入空行
     */
    QAction *act_edit_line_insert_down;

    /**
     * @brief 菜单项: 升序排列文本行
     */
    QAction *act_edit_line_asc;

    /**
     * @brief 菜单项: 升序排列整数
     */
    QAction *act_edit_line_asc_number;

    /**
     * @brief 菜单项: 升序排列小数(逗号作为小数点)
     */
    QAction *act_edit_line_asc_decimals_by_camma;

    /**
     * @brief 菜单项: 升序排列小数(句点作为小数点)
     */
    QAction *act_edit_line_asc_decimals_by_dot;



    /**
     * @brief 菜单项: 降序排列文本行
     */
    QAction *act_edit_line_desc;

    /**
     * @brief 菜单项: 降序排列证书
     */
    QAction *act_edit_line_desc_number;

    /**
     * @brief 菜单项: 降序排列小数(逗号作为小数点)
     */
    QAction *act_edit_line_desc_decimals_by_camma;

    /**
     * @brief 菜单项: 降序排列小数(句点作为小数点)
     */
    QAction *act_edit_line_desc_decimals_by_dot;


    /**
     * @brief 菜单项: 随机排列行
     */
    QAction *act_edit_line_random;








    /**
     * @brief 菜单项: 注释/取消注释
     */
    QMenu *menu_edit_comment;



    /**
     * @brief 菜单项: 添加/删除行注释
     */
    QAction *act_edit_comment_comment;

    /**
     * @brief 菜单项: 设置行注释
     */
    QAction *act_edit_comment_line;

    /**
     * @brief 菜单项: 取消行注释
     */
    QAction *act_edit_comment_line_cancle;

    /**
     * @brief 菜单项: 区块注释
     */
    QAction *act_edit_comment_block;

    /**
     * @brief 菜单项: 清除区块注释
     */
    QAction *act_edit_comment_block_cancle;




    /**
     * @brief 菜单项: 自动完成
     */
    QMenu *menu_edit_complete;

    /**
     * @brief 菜单项: 函数自动完成
     */
    QAction *act_edit_complete_func;

    /**
     * @brief 菜单项: 单词自动完成
     */
    QAction *act_edit_complete_word;

    /**
     * @brief 菜单项: 函数参数提示
     */
    QAction *act_edit_complete_param;

    /**
     * @brief 菜单项: 路径补全
     */
    QAction *act_edit_complete_path;
    /**
     * @brief 菜单项: 转为 Windows (CR LF)
     */
    QAction *act_edit_change_format_windows;

    /**
     * @brief 菜单项: 转为 Unix (LF)
     */
    QAction *act_edit_change_format_unix;

    /**
     * @brief 菜单项: 转为 Macintosh (CR)
     */
    QAction *act_edit_change_format_macintosh;
    /**
     * @brief 菜单项: 文档格式转换
     */
    QMenu *menu_edit_change_format;



    /**
     * @brief 菜单项: 空白字符操作
     */
    QMenu *menu_edit_empty;


    /**
     * @brief 菜单项: 移除行尾空格
     */
    QAction *act_edit_empty_remove_space_end;

    /**
     * @brief 菜单项: 移除行首空格
     */
    QAction *act_edit_empty_remove_space_start;

    /**
     * @brief 菜单项: 移除行首和行尾空格
     */
    QAction *act_edit_empty_remove_space_start_and_end;

    /**
     * @brief 菜单项: EOL转空格
     */
    QAction *act_edit_empty_eof_to_space;

    /**
     * @brief 菜单项: 移除非必须的空白和EOL
     */
    QAction *act_edit_empty_remove_unnecessary_space_and_eof;

    /**
     * @brief 菜单项: TAB转空格
     */
    QAction *act_edit_empty_tab_to_space;

    /**
     * @brief 菜单项: 空格转TAB(全部)
     */
    QAction *act_edit_empty_space_to_tab;

    /**
     * @brief 菜单项: 空格转TAB(行首)
     */
    QAction *act_edit_empty_space_to_tab_start;


    /**
     * @brief 菜单项: 选择性黏贴
     */
    QMenu *menu_edit_selected_paste;

    /**
     * @brief 菜单项: 黏贴HTML
     */
    QAction *edit_selected_paste_html;

    /**
     * @brief 菜单项: 黏贴RTF
     */
    QAction *edit_selected_paste_rtf;

    /**
     * @brief 菜单项: 复制二进制
     */
    QAction *edit_selected_paste_bin_copy;

    /**
     * @brief 菜单项: 剪切二进制
     */
    QAction *edit_selected_paste_bin_cut;

    /**
     * @brief 菜单项: 黏贴二进制
     */
    QAction *edit_selected_paste_bin_paste;


    /**
     * @brief 菜单项: 对选中内容操作
     */
    QMenu *menu_edit_selected_op;

    /**
     * @brief 菜单项: 打开文件
     */
    QAction *act_edit_selected_op_open_file;

    /**
     * @brief 菜单项: 在文件管理器中打开包含文件
     */
    QAction *act_edit_selected_op_open_explore;

    /**
     * @brief 菜单项: 在线搜索
     */
    QAction *act_edit_selected_op_search;

    /**
     * @brief 菜单项: 更改搜索引擎
     */
    QAction *act_edit_selected_op_change_search_engine;

    /**
     * @brief 菜单项: 列快模式
     */
    QAction *act_edit_column_model;

    /**
     * @brief 菜单项: 列快编辑
     */
    QAction *act_edit_column_edit;

    /**
     * @brief 菜单项: 字符面板
     */
    QAction *act_edit_char_panel;

    /**
     * @brief 菜单项: 历史剪切板
     */
    QAction *act_edit_clipboard_history;

    /**
     * @brief 菜单项: 设为只读
     */
    QAction *act_edit_set_readonly;

    /**
     * @brief 菜单项: 清除只读标记
     */
    QAction *act_edit_clear_readonly;





























    /**
    * @brief 菜单: 搜索
    */
    QMenu *menu_search;

    /**
    * @brief 菜单: 视图
    */
    QMenu *menu_view;

    /**
    * @brief 菜单: 编码
    */
    QMenu *menu_encoding;

    /**
    * @brief 菜单: 语言
    */
    QMenu *menu_language;

    /**
    * @brief 菜单: 设置
    */
    QMenu *menu_setting;

    /**
    * @brief 菜单: 工具
    */
    QMenu *menu_tool;

    /**
    * @brief 菜单: 宏
    */
    QMenu *menu_macro;

    /**
    * @brief 菜单: 运行
    */
    QMenu *menu_run;

    /**
    * @brief 菜单: 插件
    */
    QMenu *menu_plugin;

    /**
    * @brief 菜单: 窗口
    */
    QMenu *menu_window;



};
#endif // MAINWINDOW_H
